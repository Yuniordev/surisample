import React, {Component} from "react";
import {Link, Redirect, Route} from "react-router-dom";

import logo from "./logo.svg";
import logo2 from "./logo.png";
import "./App.css";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Items from "./pages/Items";

export default class App extends Component {

	isCurrentRoute = (route) => {
		return window.location.pathname.indexOf(route) !== -1 ? "active" : "";
	};

	render() {
		return (
			<div className="App">
				<div className="App-header">
					<img src={logo} className="App-logo" alt="logo" style={{float:"left"}}/>
					<img src={logo2} className="App-logo" alt="logo" width="10%" style={{float:"right"}}/>
					<h2>Suri Sample App</h2>
				</div>
				<nav className="navbar navbar-inverse">
					<div className="container-fluid">
						<div className="navbar-header">
							<Link to="/home" className="navbar-brand">
								Suri Sample App
							</Link>
						</div>
						<ul className="nav navbar-nav">
							<li className={this.isCurrentRoute("home")}>
								<Link to="/home">
									Home
								</Link>
							</li>
							<li className={this.isCurrentRoute("items")}>
								<Link to="/items">
									Items
								</Link>
							</li>
							<li className={this.isCurrentRoute("register")}>
								<Link to="/register">
									Register
								</Link>
							</li>
						</ul>
					</div>
				</nav>

				<div className="container-fluid">
					<Route exact path="/home" render={() => <Home/>}/>
					<Route path="/register" component={Register}/>
					<Route path="/items" component={Items}/>
				</div>
			</div>
		);
	}
}
