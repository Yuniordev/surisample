/**
 * Created by rodrigo on 07/05/17.
 */
import React, {Component} from "react";

export default class Item extends Component {

	constructor() {
		super();

		this.state = {
			items: [
				{
					id: 1,
					name: "My Little Pony Boladona",
					srcImage: "https://media.giphy.com/media/E0KEeujejKc0g/giphy.gif"
				},
				{
					id: 2,
					name: "Thor de cueca",
					srcImage: "https://rossvross.files.wordpress.com/2014/08/lady_thor_cosplay_01.jpg"
				}
			]
		}
	}

	fetchItem = (id) => {
		return this.state.items[id - 1];
	};

	render() {
		const currentItem = this.fetchItem(this.props.match.params.itemId);

		return (
			<div id="item" className="tab-pane fade in active">
				<h3>This is a boladones item.</h3>
				<h4>Id: {currentItem.id}</h4>
				<h4>Name: {currentItem.name}</h4>
				<img src={currentItem.srcImage}/>
			</div>
		);
	}
}
