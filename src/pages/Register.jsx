/**
 * Created by rodrigo on 07/05/17.
 */
import React, {Component} from "react";

export default class Register extends Component {
	/*
	 * Life Cycles
	 *
	 * 1. Component creation life cycles
	 */

	//1
	constructor() {
		super();
		console.log("1. constructor");

		this.state = {
			users: [
				{
					name: "Mesquitao",
					email: "mesquitao@suri.com.br"
				},
				{
					name: "Rodrigao",
					email: "rodrigao@suri.com.br"
				}
			],
		};
	}

	//2
	componentWillMount() {
		console.log("2. componentWillMount");
	}

	//3 and 8
	render() {
		console.log("3/8. render");
		return this.getRenderContent();
	}

	//4
	componentDidMount() {
		console.log("4. componentDidMount");
	}


	/*
	 * Life Cycles
	 *
	 * 2. Component update life cycles
	 */

	//5
	componentWillReceiveProps(nextProps) {
		console.log("5. componentWillReceiveProps");
	}

	//6
	shouldComponentUpdate(nextProps, nextState) {
		console.log("6. shouldComponentUpdate");
		return true;
	}

	//7
	componentWillUpdate(nextProps, nextState) {
		console.log("7. componentWillUpdate");
	}

	//9
	componentDidUpdate() {
		console.log("9. componentDidUpdate");
	}

	/*
	 * Life Cycles
	 *
	 * 3. Component unmount life cycles
	 */

	//10
	componentWillUnmount() {
		console.log("10. componentWillUnmount");
	}


	/*
	 * Handlers
	 */

	submitForm = (e) => {
		e.preventDefault();

		if (this.name.value && this.email.value) {
			const users = this.state.users;
			users.push({name: this.name.value, email: this.email.value});

			this.setState({
				users
			});
			this.form.reset();
		}
	};

	removeUser = (elm) => {
		this.setState({
			users: this.state.users.filter(e => e.email !== elm.email)
		});
	};


	/*
	 * Render Content
	 */
	getRenderContent = () => <div>
		<div className="col-md-5">
			<h3>Register Form</h3>
			<form className="form-horizontal" onSubmit={this.submitForm} ref={(input) => this.form = input}>
				<div className="form-group">
					<label className="control-label col-sm-2" htmlFor="name">Name:</label>
					<div className="col-sm-10">
						<input type="text" className="form-control" name="name" id="name"
						       placeholder="Your name"
						       ref={(input) => this.name = input}/>
					</div>
				</div>
				<div className="form-group">
					<label className="control-label col-sm-2" htmlFor="email">Email:</label>
					<div className="col-sm-10">
						<input type="email" className="form-control" name="email" id="email"
						       placeholder="Your email"
						       ref={(input) => this.email = input}/>
					</div>
				</div>

				<div className="form-group">
					<div className="col-sm-offset-2 col-sm-10">
						<button type="submit" className="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>
		</div>

		<div className="col-md-2">
		</div>

		<div className="col-md-5">
			<h3>Users</h3>

			<table className="table table-striped">
				<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th className="text-center">Actions</th>
				</tr>
				</thead>
				<tbody>
				{this.state.users.map((e) =>
					<tr key={e.email}>
						<td className="text-left">{e.name}</td>
						<td className="text-left">{e.email}</td>
						<td className="text-center">
							<button type="button" onClick={() => this.removeUser(e)}>
								&times;
							</button>
						</td>
					</tr>
				)}
				</tbody>
			</table>
		</div>
	</div>;
}
