import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter as Router} from "react-router-dom";
import App from "./App";

import {Provider} from "react-redux";
import {createStore} from "redux";
import {createLogger} from "redux-logger";
import combinedReducers from "./reducers/index.";
import {applyMiddleware} from "redux";


//Creating redux middleware s
const getMiddlewares = () => {
	return applyMiddleware(createLogger())
};


//Creating store and history
const store = createStore(combinedReducers, getMiddlewares());


const Root = (
	<Provider store={store}>
		<Router>
			<App/>
		</Router>
	</Provider>
);

ReactDOM.render(Root, document.getElementById('root'));
