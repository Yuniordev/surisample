import {i18nState} from 'redux-i18n';
import {combineReducers} from 'redux';

import user from './user';

export default combineReducers({
	user
});
