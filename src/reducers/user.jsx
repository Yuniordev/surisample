const defaultState = {};

export default (state = defaultState, action) => {
	switch (action.type) {
		case 'CONNECT_SOCIAL_ACCOUNT':
			return {
				...state,
				lastConnectedSocialProvider: action.payload ? action.payload.data : null,
				lastDisconnectedConnectedSocialProvider: null,
			};
		case 'DISCONNECT_SOCIAL_ACCOUNT':
			return {
				...state,
				lastDisconnectedConnectedSocialProvider: !action.payload ? null : action.payload.data,
				lastConnectedSocialProvider: null
			};
		case 'REQUEST_EMAIL_CHANGE':
			return {
				...state,
				requestedEmailChange: !!(action.payload && action.payload.data),
				changeMailToken: action.payload && action.payload.data ? action.payload.data : null,
			};
		case 'CANCEL_EMAIL_CHANGE':
			return {
				...state,
				changeMailToken: null,
				requestedEmailChange: false,
				newEmailPending: null
			};
		case 'FETCH_HAS_PENDING_TOKEN_EMAIL_CHANGE':
			let {token, hasPendingTokenEmailChange, newEmailPending} = action.payload ? action.payload.data : null;

			return {
				...state,
				changeMailToken: token,
				requestedEmailChange: hasPendingTokenEmailChange,
				newEmailPending
			};
		case 'FETCH_TOTP_STATUS':
			return {
				...state,
				totpAlreadyEnabled: action.payload ? action.payload.data : false,
			};
		case 'CREATE_TOTP_SECRET':
			return {
				...state,
				totpBase32Secret: action.payload ? action.payload.data : null
			};
		case 'CONFIRM_ENABLE_TOTP':
			return {
				...state,
				totpAlreadyEnabled: action.payload ? action.payload.data : false
			};
		case 'DISABLE_TOTP':
			return {
				...state,
				totpAlreadyEnabled: false,
				totpBase32Secret: null
			};
		case 'TOTP_PAGE_UNLOADED':
			return {
				...state,
				totpAlreadyEnabled: null,
				totpBase32Secret: null
			};
		default:
	}

	return state;
};
