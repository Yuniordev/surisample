Luiz Mesquita e Rodrigo Tessarollo:
Batalha épica:
React vs Angular vs VueJS vs JSF
Eca Script 6
Cara crachá de confiança
Mundo móvel
Mitando no teste: parte 2








Batalha épica: REACT VS. ANGULARJS VS. VUE.JS

* AngularJS: Atualmente é o lider absoluto dos frameworks SPA em uso. Mas, se tornará legado em breve a favor do Angular2, 3, 4, 5 ... 123, 124 ... É um framework da geração passada.
* AngularX : Completamente reescrito da V1. Consideramos que tinha um código e um conceito mais complexo do que os concorrentes e, na primeira POC, já o descartamos.
* VueJS 2  : Ë o mais novo framwork dessa nova geração, traz os mesmos conceitos do React, tem um código mais simples do que o do React, mas embora tenha uma comunidade em crescimento constante, achamos que o React tem mais potencial pela idade e pelo mantenedor.

* ReactJs: É um framerwork muito popular e com uma grande comunidade trabalhando a favor, tem certa curva de aprendizado inicial, mas que tende a ficar cada vez mais simples a medida que se entende os conceitos, tem o Facebook como mantenedor, aprendeu com os erros do AngularJs, tem muitos cases de sucesso como: Instagram, WhatsApp Web.


* JSF: 
	Segundo a definição: JSF é uma tecnologia que nos permite criar aplicações Java para Web utilizando componentes visuais pré-prontos, de forma que o desenvolvedor não se preocupe com Javascript e HTML. Basta adicionarmos os componentes (calendários, tabelas, formulários) e eles serão renderizados e exibidos em formato html.

	Ou seja, é algo completamente diferente dos frameworks SPA citados anteriormente, tem uma série de vantagens e desvantagens, baseado na experiência de cada dev.
	Como desvantagem: Abstrai muito a Web, Mais dificil escalar, maior consumo de recursos computacionais ao manter todo os estado do cliente no servidor, mais dificil customizar.
	Como vantagem: Abstrai muito a Web, Possui muitas bibliotecas de componentes visuais que ajudam a criar telas, etc.


* CLICHE TIME:
A verdade é que cada biblioteca ou framework tem suas vantagens e desvantagens. Você precisa decidir qual a melhor, baseado na necessidade da sua aplicação e time de desenvolvimento, ou seja, cada caso é um caso, e não existe biblioteca ou framework perfeito para todos os casos.





React - https://tableless.com.br/react-javascript-reativo/

* REACT é uma biblioteca JAVASCRIPT para desenvolvimento de interfaces de usuário. Facebook boladão foi quem desenvolveu.
* APENAS interface
* Usa os bons e velhos conhecimentos de HTML, CSS, JSON e Javascript que tu já tem (ou deveria ter).
* Na maior parte do tempo, você estará escrevendo JavaScript puro. 
* Quanto mais Javascript você tiver, melhor. 
* Virtual DOM: tem uma cópia do DOM REAL em memória para otimizar o desempenho do app.


Javascript declarativo

* Te ajuda a focar mais no resultado do que na forma como ele é atingido. 
* Escrever menos e ter uma aplicação funcional com menor esforço e menor tempo.
* Ciclo de vida bem definido e de facil compreensão.


Javascript reativo

* Todo componente React possui dois atributos principais: estado (state) e propriedades (props). 
* State: É de dominio do componente, pode ser usado pra controlar algo condicional durante a execução do mesmo.
* Props: Parecido com o state, com a diferença de ser READ-ONLY (pode ter o valor alterado via ACTIONS do REDUX (Mais adiante)), é a forma de um componente PAI falar com um FILHO.
* Toda vez que o estado de um componente é alterado, ele é re-renderizado automaticamente.
* Isto é JavaScript Reativo
* No momento em que o compoente for re-renderizado, o Virtual DOM é consultado para se evitar desnecessarias manipulações no DOM Real.


JSX

* JSX é uma extensão da sintaxe do JavaScript. Lembra XML, HTML mas na real é uma mistura de JavaScript, HTML, JSON e cruza de crocodilo com urubu.
* IMPORTANTE: Teu 'sélebro' vai dar uma travada no inicio. Entretanto, ela é opcional.
* JSX pode ser utilizado com qualquer código JavaScript, não só React

* Exemplo JSX:

	const jsxBoladao = <div> 
							<ul>
								<li>Mesquitao <3 Rodrigao</li>
								<li>{this.chamadaMetodo()}</li>
								<li>{this.props.algumProp}</li>
								<li>{this.state.algumState}</li>
								<li>{true ? 'condicional' : 'aqui'}</li>
							</ul>
					   </div>;


   const jsxBoladao2 = <AlgumComponenteSeu label="Texto 1" onAlgumEvento={this.callBackBoladao}/>;










Ecma Script 6


* O ECMAScript (ES) é a especificação da linguagem de script que o JavaScript implementa, ou seja, é a descrição formal e estruturada de uma linguagem de script, sendo padronizada pela Ecma International – associação criada em 1961 dedicada à padronização de sistemas de informação e comunicação – na especificação ECMA-262. 

* No dia 17 de junho de 2015, foi definida a sexta edição da especificação, a ES6 (também chamada de ECMAScript 2015).

* E simplesmente muito, mas muito legal.

* https://github.com/lukehoban/es6features/blob/master/README.md


* Arrow Functions:
  Uma expressão arrow function possui uma síntaxe mais curta quando comparada com expressões de função (function expressions) e vincula o valor de this de maneira léxica. Arrow functions sempre são anônimas.

  Ex:
  let a = ["Hidrogenio","Helio"];
  let anonima = a.map(function(s){ return s.length });
  let arrowFu = a.map( s => s.length );

  this léxico

  Antes das arrow functions, toda nova função definia seu próprio valor de this (um novo objeto no caso de um construtor, undefined em chamadas de funções com strict mode, o objeto de contexto se a função é chamada como um "método de objeto", etc.). Este comportamento é importuno com um estilo de programação orientado a objeto.


  Ex:

  Antes das Arrows:

  function Pessoa() {
  // O contrutor Pessoa() define `this` a si próprio.
  this.idade = 0;

  setInterval(function crescer() {
    // Em modo não estrito, a função crescer() define `this` 
    // como o objeto global, que é diferente ao `this`
    // definido pelo construtor de Pessoa().
    this.idade++;
  }, 1000);
}

var p = new Pessoa();



No ECMAScript 3/5, este comportamento era corrigido definindo o valor em this à uma variável que pudesse ser encapsulada.

function Pessoa() {
  var self = this; // Alguns escolhem `that` ao invés de `self`. 
                   // Escolha uma e seja consistente.
  self.idade = 0;

  setInterval(function crescer() {
    // O callback referência a variável `self` a qual
    // o valor é o objeto esperado.
    self.idade++;
  }, 1000);
}


Depois das Arrows:

function Pessoa(){
  this.idade = 0;

  setInterval(() => {
    this.idade++; // |this| corretamente referência ao objeto Pessoa
  }, 1000);
}

var p = new Pessoa();



* Classes

	class Catiorro extends Animal {
	    construtor(barulho) {
 			super(barulho);

 			this.barulho = barulho;
 			this.name = "Catiorrineo"
	    }

		get name() {
    		return this.name;
  		}
  
  		set name(name) {
    		this.name = name;
 		}

	    barulhar() {
	    	super.barulhar();
	    }
	}


* Template Strings	

  Template strings são envolvidas por crases (` `) em vez de aspas simples ou duplas. Template strings podem possuir marcadores. Estes são indicados pelo sinal de dólar seguido de chaves (${expression}).	

  Ex:
  var a = 5;
  var b = 10;
  console.log("Quinze é " + (a + b) + " e não " + (2 * a + b) + ".");   // "Quinze é 15 e não 20."	
  console.log(`Quinze é ${a + b} e não ${2 * a + b}.`);   // "Quinze é 15 e não 20."	


* Destructuring - Atribuição via desestruturação

  A sintaxe de atribuição via desestruturação (destructuring assignment) é uma expressão JavaScript que possibilita extrair dados de arrays ou objetos em variáveis distintas.

  Ex:
  var test = {a:1, b:2}
  var foo = ["one", "two", "three"];

var [one, two, three] = foo;
console.log(one); // "one"
console.log(two); // "two"
console.log(three); // "three"

var {a, b} = test;
console.log(a); // "1"
console.log(b); // "2"



* Outras novidades: Modules, Promises, Default + Rest + Spread, Let + Const













Cara crachá de confiança - JWT - JSON Web Token

O JWT é um padrão aberto RFC que define uma forma segura, autocontida e compacta de envio de dados entre partes, esta informação pode ser verificada e confiável pois é assinada digitalmente. O JWT pode ser assinado usando um algoritmo (HMAC) ou uma chave pública/privada usando RSA.


Vamos para as definições:

compacta: Por ser extremamente leve o JWT é perfeito para APIs, obviamente a sua transmissão na rede é bem rápida, o que também significa dizer que para dispositivos que demandam uma banda limitada de rede, o consumo de dados para autenticar um cliente é sumariamente reduzido em relação ao outra abordagem.

autocontida: A definicão trás um termo chamado payload que contém os dados do usuário:

Exemplo:

{
  "id":"abc6sa245ds6546agq231",
  "name":"Mesquitao Mestre da Treta Nascimento",
  "username":"mesq",
  "roles":true
}

Outro termo usado no payload são as claims (ou reenvidicações) que são usadas para autorizar o envio de mensagem atráves do token, as claims são os dados do usuário, por isso o termo usado no payload é autocontido, porque contém por si só os dados que precisam ser usados.

A estrutura do JWT é semelhante a essa:

xxxxx.yyyyy.zzzzz

Essa estrutura é basicamente separada por pontos . e cada ponto indica algo, mostrada abaixo:

cabeçalho-->xxxxx;
payload-->yyyyy;
signature-->zzzzz;


E assinatura? O que ela significa?

A assinatura é um agoritmo baseado no secret que é definido no Backend da sua aplicação e criptografado randomicamente em base-64, você pode usar outra forma de criptografar essa assinatura, como HMAC-SHA256, inclusive. A grande importância da assinatura é que ela permite que o token seja imodificável durante sua transmissão pela rede.

Imagem:
https://github.com/Webschool-io/be-mean/raw/master/Apostila/module-nodejs/pt-br/images/jwt-schema-png.png







Mundo móvel - React Native

O React Native é um projeto desenvolvido pelos engenheiros do Facebook e que consiste em uma série de ferramentas que viabilizam a criação de aplicações móveis nativas para a plataforma iOS e Android.

O stack do React Native é poderoso, pois nos permite utilizar ECMAScript 6, CSS Flexbox, JSX, diversos pacotes do NPM e muito mais. Sem contar que nos permite fazer debug na mesma IDE utilizada para o desenvolvimento nativo com essas plataformas (além de tornar o processo extremamente divertido).

Uma das grandes vantagens é utilizar o React como codigo base para o desenvolvimento dos apps, o que permite reusar o conhecimento previo em React.












EXEMPLO FUNCIONAL REACT

	<Implementado >
